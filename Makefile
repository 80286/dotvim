update_coc:
	cd pack/bundle/opt/coc.nvim && yarn install

update_fzf:
	cd pack/bundle/opt/fzf && make && make install
	cd nvim/pack/bundle/opt/telescope-fzf-native.nvim && make

update: update_coc update_fzf

all: update
