#!/bin/bash

# https://scalameta.org/metals/docs/editors/vim

function update_metals() {
    local version="$1"

    if [ -z "${version}" ]; then
        echo "Expected version"
        return
    fi

    curl -L -o coursier https://git.io/coursier-cli
    chmod +x coursier

    ./coursier bootstrap \
      --java-opt -Dmetals.client=nvim-lsp \
      org.scalameta:metals_2.13:$version \
      -o /usr/local/bin/metals-vim -f
}

update_metals "1.5.1"
