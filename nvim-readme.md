### Installation

1. Install neovim from system pkg manager or build from [https://github.com/neovim/neovim]
2. Clone this repo and move it for example to: `~/.vim`
3. Go to root dir of cloned git repo and run `git submodule update --init --recursive`
4. Link `~/.config/vim` to git root dir of this repo otherwise nvim won't find your config
5. Optionally: run `./update_metals`
6. Optionally: run `make update_fzf`
7. Install tree-sitter cli [https://github.com/tree-sitter/tree-sitter/blob/master/cli/README.md]
8. Launch `nvim` and make sure there are no runtime errors
9. In nvim: run `:TSUpdate`
