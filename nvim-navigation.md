# Normal mode (N)
- F1     - show list of telescope func. -> T
- F4     - show list of private keymaps -> T

- C-h    - show list of oldfiles        -> T
- C-b    - show list of buffers         -> T
- C-n    - show list of gitfiles        -> T

- F7     - 80286.grep menu              -> T
- Alt-F7 - 80286.grep instant           -> T

# Debug (D)
- ,dt - set bp                                 -> D
- ,dc - start debugging & run dap-ui interface -> DUI

# Dap-ui interface (DUI)
- ,dr / F3 - close dap-ui interface     -> N
- ,de      - evaluate expr under cursor -> eval window -> F3 -> DUI

# Telescope interface (T)
- C-x - open in hsplit        -> N
- C-v - open in vsplit        -> N
- C-t - open in new tab       -> N
- CR  - open in 'this' window -> N
