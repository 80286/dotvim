" Vim syntax file
" Language:	Zdoom DECORATE files
" Maintainer:	dx286 <dx286@tlen.pl>
" Last change:	22.09.2013

if !exists("main_syntax")
	if version < 600
    		syntax clear
	elseif exists ("b:current_syntax")
	finish
endif
	let main_syntax = 'decorate'
endif

syn match decorate_goto "[Gg]oto \w*"
syn match decorate_func "\(A\|ACS\)_\S\+"
syn match decorate_func "\(A\|ACS\)_[^( ]\+"
syn match multiple "*"
syn match decorate_mflag "-[A-Za-z]\+\([.A-Za-z]\+\)\?"
syn match decorate_pflag "+[A-Za-z]\+\([.A-Za-z]\+\)\?"
syn match decorate_state "\S\+:"
"syn match decorate_int "[^[:alpha:]][-+]\?\d\+"
"syn match decorate_float "[^[:alpha:]]?[-+]\?^\d\+\.\d*"

" from /usr/share/vim/vim73/syntax/c.vim
syn match	cNumbers	display transparent "\<\d\|\.\d" contains=cNumber,cFloat
" Same, but without octal error (for comments)
syn match	cNumbersCom	display contained transparent "\<\d\|\.\d" contains=cNumber,cFloat
syn match	cNumber		display contained "\d\+\(u\=l\{0,2}\|ll\=u\)\>"
"hex number
syn match	cNumber		display contained "0x\x\+\(u\=l\{0,2}\|ll\=u\)\>"
" Flag the first zero of an octal number as something special
syn match	cFloat		display contained "\d\+f"
"floating point number, with dot, optional exponent
syn match	cFloat		display contained "\d\+\.\d*\(e[-+]\=\d\+\)\=[fl]\="
"floating point number, starting with a dot, optional exponent
syn match	cFloat		display contained "\.\d\+\(e[-+]\=\d\+\)\=[fl]\=\>"
"floating point number, without dot, with exponent
syn match	cFloat		display contained "\d\+e[-+]\=\d\+[fl]\=\>"
syn case ignore
" end c.vim

"syn region decorate_quot start="\"" end="\"" keepend contains=@Spell
syn match decorate_quot "\"[^"]*\""
if exists("c_no_comment_fold")
	syn region decorate_comment start="//" skip="\\$" end="$" keepend contains=@Spell
else
	syn region decorate_comment start="//" skip="\\$" end="$" keepend contains=@Spell fold extend
endif
syn region decorate_comment matchgroup=decorate_comment start="/\*" end="\*/" contains=@Spell fold extend
syn keyword decorate_tag0 actor states stop
syn keyword decorate_tag1 replaces bright loop
syn keyword decorate_tag2 projectile monster skip_super
syn match decorate_rand "f\?random \?"
syn match decorate_include "#include"

if version >= 508 || !exists("did_decorate_syn_inits")
	if version < 508
		let did_decorate_syn_inits = 1
		command -nargs=+ HiLink hi link <args>
	else
		command -nargs=+ HiLink hi def link <args>
	endif
	HiLink decorate_comment Comment

	HiLink decorate_goto Label
	HiLink decorate_func Macro
	HiLink decorate_func_b Macro
	HiLink multiple Constant
	HiLink decorate_mflag Special
	HiLink decorate_pflag Macro
	HiLink decorate_state Label

	HiLink cNumber Special
	HiLink cFloat Special

	HiLink decorate_quot Constant
	HiLink decorate_tag0 Label
	HiLink decorate_tag1 Repeat
	HiLink decorate_tag2 Label
	HiLink decorate_rand Macro
	HiLink decorate_include Include

	delcommand HiLink
endif

let b:current_syntax = "decorate"

if main_syntax == 'decorate'
	unlet main_syntax
endif

" vim: ts=8
