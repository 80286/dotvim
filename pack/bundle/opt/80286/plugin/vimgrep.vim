fun! Vimgrep(...)
    cexpr system("python3 $HOME/bin/vimgrepExt ".join(a:000)." -N")
    copen
endfunction

" vimgrep quickfix version:
command! -complete=dir -nargs=+ Vimgrep call Vimgrep('<args>')

function! VimgrepGetCommand(patternToSearch, args, line)
    let l:vimgrepJsonCfgId = trim(split(a:line, ";")[0])
    let l:cmd = "python3 ~/bin/vimgrepExt -t " . l:vimgrepJsonCfgId
    let l:cmd = l:cmd . " " . a:args . " -e '" . a:patternToSearch . "'"
    return l:cmd
endfunction

function! VimgrepCfgSink(patternToSearch, line)
    let l:vimgrepCmd = VimgrepGetCommand(a:patternToSearch, "", a:line)
    call fzf#vim#grep(l:vimgrepCmd, 0, {})
endfunction

function! VimgrepCfgQfSink(patternToSearch, line)
    let l:vimgrepCmd = VimgrepGetCommand(a:patternToSearch, "-N", a:line)
    cexpr system(l:vimgrepCmd)
    copen
    let w:quickfix_title = l:vimgrepCmd
endfunction

function! VimgrepCfgBase(toQfWindow, patternToSearch)
    let l:cmdPrefix = "python3 ~/bin/"
    let l:vimgrepExtPath = l:cmdPrefix . "vimgrepExt "
    let l:findUpwardPath = l:cmdPrefix . "b_findUpward.py "

    let l:configDir = trim(system(l:findUpwardPath . '.vimgreprc ' . getcwd()))

    if l:configDir == ""
        let l:nearestGitDirPath = trim(system(l:findUpwardPath . '.git ' . getcwd()))
        let l:vimgrepCmd = ""

        if l:nearestGitDirPath == ""
            let l:vimgrepCmd = l:vimgrepExtPath . getcwd() . ' --no-ignore -e ' . a:patternToSearch
        else
            let l:nearestGitDir = trim(system('dirname ' . l:nearestGitDirPath))
            let l:vimgrepCmd = l:vimgrepExtPath . l:nearestGitDir . ' -G -e ' . a:patternToSearch
        endif

        if a:toQfWindow == 0
            call fzf#vim#grep(l:vimgrepCmd, 0, {})
        else
            cexpr system(l:vimgrepCmd)
            copen
            let w:quickfix_title = l:vimgrepCmd
        endif
    else
        let l:SinkFunctionName = "VimgrepCfgSink"

        if a:toQfWindow == 1
            let l:SinkFunctionName = "VimgrepCfgQfSink"
        endif

        call fzf#run({'source': l:vimgrepExtPath . '-N -p', 'sink': function(l:SinkFunctionName, [a:patternToSearch])})
    endif
endfunction

nnoremap <F7> :call VimgrepCfgBase(0, expand('<cword>'))<CR>
vnoremap <F7> :call VimgrepCfgBase(0, getreg('*'))<CR>
nnoremap <S-F7> :call VimgrepCfgBase(1, expand('<cword>'))<CR>
vnoremap <S-F7> :call VimgrepCfgBase(1, getreg('*'))<CR>
