function! SelectionToQuickfix() range
    let v = @*
    cexpr v
    copen
endfunction
command! -range -nargs=0 ToQuickfix <line1>,<line2>call SelectionToQuickfix()
