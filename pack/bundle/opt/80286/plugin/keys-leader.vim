" Open vimrc in new tab:
nmap <leader>v :tabedit $MYVIMRC<CR>

" Align visual selection
vmap <leader>= :Tabularize /=

" LSP <Leader> defs: {{{
nmap <leader>rn           <Plug>(ide-lsp-rename)

" Formatting selected code.
xmap <leader>f            <Plug>(ide-lsp-format-selected)
nmap <leader>f            <Plug>(ide-lsp-format-selected)

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a            <Plug>(ide-lsp-codeaction-selected)
nmap <leader>a            <Plug>(ide-lsp-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac           <Plug>(ide-lsp-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf           <Plug>(ide-lsp-fix-current)
" }}}

" Debugger <Leader> defs: {{{
nmap <buffer> <leader>dd  <Plug>(ide-debugger-run)
nmap <buffer> <leader>dc  <Plug>(ide-debugger-close)
nmap <buffer> <leader>db  <Plug>(ide-debugger-toggle-breakpoint)
nmap <buffer> <leader>dcb <Plug>(ide-debugger-clear-all-breakpoints)
nmap <buffer> <leader>drc <Plug>(ide-debugger-run-to-cursor)

nmap <buffer> <leader>dso <Plug>(ide-debugger-step-over)
nmap <buffer> <leader>dsi <Plug>(ide-debugger-step-into)
nmap <buffer> <leader>dso <Plug>(ide-debugger-step-out)
" }}}


