let s:path = expand('<sfile>:p:h')
let s:bundleConfigDir = s:path . '/../bundleConfig/'
let g:bundle_dirs = [expand(g:initialVimDirectory . '/pack/bundle/opt')]
" let g:bundle_dirs = get(g:, 'bundle_dirs', [])

try
  if empty(g:bundle_dirs)
    throw 'Empty g:bundle_dirs'
  endif

  call util#bundle#loadCustomConfigurations(g:bundle_dirs, s:bundleConfigDir)
  set termguicolors

  " call util#term#loadConfiguration(s:path . '/../termConfig')
catch /^Vim\%((\a\+)\)\=:E117/
  echomsg v:exception
endtry

set updatetime=300
