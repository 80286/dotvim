nmap <buffer> <leader>dd  <Plug>(ide-debugger-run)
nmap <buffer> <leader>dc  <Plug>(ide-debugger-close)
nmap <buffer> <leader>db  <Plug>(ide-debugger-toggle-breakpoint)
nmap <buffer> <leader>dcb <Plug>(ide-debugger-clear-all-breakpoints)
nmap <buffer> <leader>drc <Plug>(ide-debugger-run-to-cursor)

nmap <buffer> <leader>dso <Plug>(ide-debugger-step-over)
nmap <buffer> <leader>dsi <Plug>(ide-debugger-step-into)
nmap <buffer> <leader>dso <Plug>(ide-debugger-step-out)
