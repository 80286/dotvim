if has("autocmd")
    autocmd FileType man nnoremap <buffer> q :quit<CR>
    autocmd FileType man vnoremap <buffer> q <C-c>:quit<CR>
    autocmd FileType man nnoremap <buffer> <space> <C-f>
    autocmd FileType man set ignorecase smartcase nomod noma
    autocmd FileType man set nonumber
    autocmd FileType help wincmd _
    autocmd FileType sh set isfname-==
end

au BufNewFile,BufRead svn-log.* setf svn
au BufRead,BufNewFile *.lmp set filetype=decorate
au BufRead,BufNewFile grub.cfg set foldmethod=marker | set foldmarker={,}
