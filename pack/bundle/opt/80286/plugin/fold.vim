set foldmethod=marker
set foldcolumn=1
set modeline modelines=5
hi Folded term=standout ctermbg=DarkGrey ctermfg=White guibg=#808080 guifg=DarkBlue

" hi Folded term=standout ctermbg=DarkGrey ctermfg=White guibg=#808080 guifg=DarkBlue
" hi FoldColumn term=standout ctermbg=DarkGrey ctermfg=White guibg=#808080 guifg=DarkBlue

" 0x7A69Dark:
" hi Folded	ctermfg=darkgrey ctermbg=NONE
" hi FoldColumn	ctermfg=darkgrey ctermbg=NONE
