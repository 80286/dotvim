" https://www.reddit.com/r/vim/comments/74eapd/random_characters_on_startup_with_set/
set t_SH=
set t_RS=
set t_u7=
set t_RV=

" Fix not working ctrl/shift keys in fzf input:
" https://github.com/junegunn/fzf.vim/issues/900#issuecomment-565821765
let &t_TI = ""
let &t_TE = ""

" https://github.com/kovidgoyal/kitty/issues/108
" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
let &t_ut=''
