" qf_toggle() {{{
function! s:qf_toggle()
    for i in range(1, winnr('$'))
        let bnum = winbufnr(i)
        if getbufvar(bnum, '&buftype') == 'quickfix'
            cclose
            return
        endif
    endfor

    copen
endfunction
command! Ctoggle call s:qf_toggle() 
" }}}
"
" Toggle quickfix
nmap ` :Ctoggle<CR>

" Skip to next window
map <Tab> <C-w>w

" b, w, v for Insert mode:
imap <C-Left> <C-o>b
imap <C-Right> <C-o>w
imap <S-Right> <ESC>v
imap <S-Left> <ESC>v

" Scroll one line up/down
noremap <S-Up> <C-y>
noremap <S-Down> <C-e>

" Call omni-completion in insert mode:
inoremap <C-z> <C-x><C-o>

" Prevent openning ex mode prompt:
nnoremap Q <nop>

" Clear highlight after search commands:
noremap <C-l> :nohl<CR>

xnoremap <C-Insert> "*y

" Open vimrc in new tab:
nmap <leader>v :tabedit $MYVIMRC<CR>

" Align visual selection
vmap <leader>= :Tabularize /=

