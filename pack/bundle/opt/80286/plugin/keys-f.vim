" Save file
imap <F2> <ESC>:w<CR>a
map  <F2> <ESC>:w<CR>

" Close Vim
map  <F3>   <ESC>:q<CR>
imap <F3>   <ESC>:q<CR>
map  <S-F3> <ESC>:q!<CR>
imap <S-F3> <ESC>:q!<CR>

" Normal mode: insert new line
map <F4> o<ESC>

" Execute script
map <F5> <ESC>:!./%<CR>

" Normal mode: Substitite word under cursor:
nnoremap <F6> :%s/<C-r><C-w>/

" Show file explorer:
map  <F8> :NERDTreeToggle %:h<CR>
imap <F8> :NERDTreeToggle %:h<CR>


" Call make (dispatch)
nmap <F9> :Dispatch make<CR>
nmap <F10> :Dispatch make run<CR>

