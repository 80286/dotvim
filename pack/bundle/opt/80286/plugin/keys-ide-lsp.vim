nmap <leader>rn             <Plug>(ide-lsp-rename)

" Formatting selected code.
xmap <leader>f              <Plug>(ide-lsp-format-selected)
nmap <leader>f              <Plug>(ide-lsp-format-selected)

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a              <Plug>(ide-lsp-codeaction-selected)
nmap <leader>a              <Plug>(ide-lsp-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac             <Plug>(ide-lsp-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf             <Plug>(ide-lsp-fix-current)

nnoremap <silent> K         <Plug>(ide-lsp-show-documentation)

nnoremap <silent> <space>a  <Plug>(ide-lsp-diagnostics)
nnoremap <silent> <space>e  <Plug>(ide-lsp-extensions)
nnoremap <silent> <space>c  <Plug>(ide-lsp-commands)
nnoremap <silent> <space>o  <Plug>(ide-lsp-outline)
nnoremap <silent> <space>s  <Plug>(ide-lsp-symbols)
nnoremap <silent> <space>j  <Plug>(ide-lsp-next)
nnoremap <silent> <space>k  <Plug>(ide-lsp-prev)
nnoremap <silent> <space>p  <Plug>(ide-lsp-resume)

nmap <silent> <C-s>         <Plug>(ide-lsp-range-select)
xmap <silent> <C-s>         <Plug>(ide-lsp-range-select)

nmap <silent> [g            <Plug>(ide-lsp-diagnostic-prev)
nmap <silent> ]g            <Plug>(ide-lsp-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd            <Plug>(ide-lsp-definition)
nmap <silent> gy            <Plug>(ide-lsp-type-definition)
nmap <silent> gi            <Plug>(ide-lsp-implementation)
nmap <silent> gr            <Plug>(ide-lsp-references)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if                     <Plug>(ide-lsp-funcobj-i)
omap if                     <Plug>(ide-lsp-funcobj-i)
xmap af                     <Plug>(ide-lsp-funcobj-a)
omap af                     <Plug>(ide-lsp-funcobj-a)
xmap ic                     <Plug>(ide-lsp-classobj-i)
omap ic                     <Plug>(ide-lsp-classobj-i)
xmap ac                     <Plug>(ide-lsp-classobj-a)
omap ac                     <Plug>(ide-lsp-classobj-a)
