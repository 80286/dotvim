" if $TERM == 'xterm-256color' || $TERM == 'screen-256color'
	" colorscheme lucius
    " colorscheme Tomorrow-Night
    " colorscheme Tomorrow-Night-Bright
    " colorscheme molokai
	" LuciusBlackLowContrast
    " LuciusLightHighContrast
    " colorscheme 0x7A69_dark
    " colorscheme morning
" endif

colorscheme molokai

highlight LineNr term=none cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

highlight DiffAdd term=reverse cterm=bold ctermbg=green ctermfg=white
highlight DiffChange term=reverse cterm=bold ctermbg=cyan ctermfg=black
highlight DiffText term=reverse cterm=bold ctermbg=gray ctermfg=black
highlight DiffDelete term=reverse cterm=bold ctermbg=red ctermfg=black 
