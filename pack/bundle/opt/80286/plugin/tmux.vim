if $TERM=='rxvt-unicode'
	imap 5a <C-Up>
	imap Od <C-Left>
	imap Oc <C-Right>
	imap 5b <C-Down>
	imap 	<C-l>
	imap [5^ <C-PageUp>
	imap [6^ <C-PageDown>
	imap [7^ <C-Home>
	imap [8^ <C-End>
	map Oa <C-Up>
	map Od <C-Left>
	map Oc <C-Right>
	map Ob <C-Down>
	map [5^ <C-PageUp>
	map [6^ <C-PageDown>
	map [7^ <C-Home>
	map [8^ <C-End>
	set <S-Up>=[a
	set <S-Down>=[b
endif

if &term =~ '^screen'
    " tmux will send xterm-style keys when its xterm-keys option is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
    map <Esc>[B <Down>
endif
