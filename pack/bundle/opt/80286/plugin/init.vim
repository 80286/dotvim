" Turn off compatibility with Vi
set nocp

filetype plugin on

" Show number of matches in statusline bar:
se shm-=S

" Syntax highlighting only for vim-enhanced
if has("syntax")
    syntax on
endif

" Show line numbers:
set number

set backupdir=~/.vim/backup/
set directory=~/.vim/backup/
let g:WriteBackup_BackupDir = '~/.vim/backup' 
set wildmenu
set history=50
set showcmd
set pastetoggle=<F11>
set nofixendofline

" Default backspace like normal
set bs=2

" Some options deactivated by default (remove the "no" to enable them)
set nobackup
set hlsearch
set incsearch

" Show the position of the cursor
set ruler

" Show matching parentheses
set noshowmatch

" Make % work with <>
set mps+=<:>
set mouse=a
set spelllang=pl

filetype on

set splitbelow
set splitright

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" https://vim.fandom.com/wiki/Set_working_directory_to_the_current_file#Automatically_change_the_current_directory
" set autochdir
" autocmd BufEnter * silent! lcd %:p:h
