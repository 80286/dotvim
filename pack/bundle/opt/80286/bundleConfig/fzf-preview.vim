function! s:PreviewFzfCommandsSink(line)
    let l:fzfCommandName = trim(split(a:line, "#")[0])
    execute ':' . l:fzfCommandName
endfunction

function! PreviewFzfCommands()
    let l:commandsPath = '~/.vim/pack/bundle/opt/80286/bundleConfig/fzf-commands'
    let l:windowOptions = { 'width': 1.0, 'height': 0.5, 'yoffset': 1.0 }

    call fzf#run({'source': 'cat ' . l:commandsPath, 'sink': function('s:PreviewFzfCommandsSink'), 'window': l:windowOptions})
endfunction

nnoremap <F1> :call PreviewFzfCommands()<CR>

