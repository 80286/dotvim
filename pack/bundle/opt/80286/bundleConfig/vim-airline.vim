" Always display a status-bar
set laststatus=2
set statusline=%-10.3n

hi FoldColumn term=standout ctermbg=DarkGrey ctermfg=White guibg=#808080 guifg=DarkBlue

" paprykarz 21.07.14
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_theme = 'kalisi'
let g:airline_inactive_collapse = 0
let g:airline_powerline_fonts = 0

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#tab_nr_type = 2
let g:airline#extensions#tabline#tab_min_count = 2

function! AirlineInit()
    let g:airline_section_a = 'b%n, w%{winnr()}%#__accent_bold#%{winnr()==winnr("#")?" [LW]":""}%#__restore__#'
endfunction

autocmd User AirlineAfterInit call AirlineInit()
