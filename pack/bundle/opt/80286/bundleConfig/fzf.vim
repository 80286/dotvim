let s:fzf_preview_path = (expand('<sfile>:p:h') . "/fzf-preview.vim")
exec "source " . s:fzf_preview_path

" Required for arctgx-grep-operator package:
let g:fzf_action = {
      \ 'enter': 'TabDrop',
      \ 'ctrl-y': 'edit',
      \ 'ctrl-t': 'tab split',
      \ 'ctrl-x': 'split',
      \ 'ctrl-v': 'vsplit'
      \ }

" fzf.vim key bindings
noremap <Plug>(zx-list-history)         :History<CR>
noremap <Plug>(zx-list-files)           :Files<CR>
noremap <Plug>(zx-list-git-files)       :GFiles<CR>
noremap <Plug>(zx-list-buffers)         :Buffers<CR>
noremap <Plug>(zx-list-grep-results)    :Rg<CR>
noremap <Plug>(zx-list-tags)            :BTags<CR>
