" Make GStatus diff in vertical window:
set diffopt=filler,vertical

" https://github.com/tpope/vim-fugitive/issues/1495
" https://vi.stackexchange.com/a/9191
cabbrev G 10split<bar>0Git
