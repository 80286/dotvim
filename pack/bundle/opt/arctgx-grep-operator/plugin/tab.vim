augroup WindowHistory
  autocmd!
  autocmd VimEnter * call arctgx#windowhistory#createFromWindowList()
  autocmd WinEnter * ++nested call arctgx#window#onWinEnter()
  autocmd QuitPre * call arctgx#window#closePopupForTab()
augroup END

if has('nvim') == 1
    augroup WindowHistory
    autocmd WinClosed * ++nested call arctgx#window#onWinClosed(str2nr(expand('<afile>')))
    augroup END
endif

command! -complete=file -nargs=+ TabDrop call arctgx#base#tabDropMulti(<f-args>)
command! -complete=file -nargs=+ T call arctgx#base#tabDropMulti(<f-args>)

nmap <Plug>(ide-close-popup) <Cmd>call arctgx#window#closePopupForTab()<CR>
