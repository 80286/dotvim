local ext = {}

function ext.getRealpath(path)
  local ok, fileDir = pcall(vim.uv.fs_realpath, path)

  if not ok or fileDir == nil then
    return assert(vim.uv.cwd())
  end

  return fileDir
end

function ext.getBufferCwd(buf)
  buf = buf or vim.api.nvim_get_current_buf()

  local bufferFileRealpath = ext.getRealpath(vim.api.nvim_buf_get_name(buf))

  return vim.fs.dirname(bufferFileRealpath)
end

function ext.git_dir(relativeDir)
  local out = vim.system({'git', 'rev-parse', '--show-toplevel'}, {cwd = relativeDir}):wait()

  if out.code > 0 then
    vim.notify(('Cannot determine top level directory for %s'):format(relativeDir), vim.log.levels.WARN, {title = 'git'})
    return relativeDir
  end

  return vim.trim(out.stdout)
end

function ext.getMinPrefix(completions, inputPrefix)
  local function getCommonPrefixForStr(s1, s2)
    local function charAt(s, idx) return string.sub(s, idx, idx) end
    local result = ""

    for i = 1, #s1 do
      if i <= #s2 and charAt(s1, i) == charAt(s2, i) then
        result = result .. charAt(s1, i)
      else
        break
      end
    end

    return result
  end

  local function commonPrefixForStrings(strings)
    local commonPrefix = strings[1]

    for i, str in ipairs(strings) do
      if i > 1 then
        commonPrefix = getCommonPrefixForStr(commonPrefix, str)
      end
    end

    return commonPrefix
  end

  if #completions == 1 then
    return completions[1]
  end

  local matchedCompletions = {}

  for _, cmp in ipairs(completions) do
    if vim.startswith(cmp, inputPrefix) then
      table.insert(matchedCompletions, cmp)
    end
  end

  if #matchedCompletions == 0 then
    return inputPrefix
  elseif #matchedCompletions == 1 then
    return matchedCompletions[1]
  elseif #matchedCompletions > 1 then
    return commonPrefixForStrings(matchedCompletions)
  end
end

-- Src: __internal.lua: command_history
function ext.getCommandHistory(filter_fn)
  local history_string = vim.fn.execute "history cmd"
  local history_list = vim.split(history_string, "\n")

  local results = {}

  for i = #history_list, 3, -1 do
    local item = history_list[i]
    local _, finish = string.find(item, "%d+ +")
    local cmd = string.sub(item, finish + 1)

    if filter_fn then
      if filter_fn(cmd) then
        table.insert(results, cmd)
      end
    else
      table.insert(results, cmd)
    end
  end

  return results
end

function ext.tableGetUniqueItems(items)
  local uniqueItems = {}
  local duplicates = {}

  for _, item in ipairs(items) do
    if duplicates[item] == nil then table.insert(uniqueItems, item) end
    duplicates[item] = 1
  end

  return uniqueItems
end

return ext
