local M = {}

local tb = require("telescope.builtin")

M.ListLastCalls = function(cmdName)
  local cmdPrefix = ("%s "):format(cmdName)

  tb.command_history({
    results_title = ("Last command_history %s calls"):format(cmdName),
    filter_fn = function(cmd) return (string.sub(cmd, 1, #cmdPrefix) == cmdPrefix) and (#cmd > #cmdPrefix) end,
    prompt_title = ""
  })
end

M.LiveGrep = function(cwd, text)
  local title = ("LiveGrep in dir: %s"):format(cwd)

  tb.live_grep({
    cwd = cwd,
    preview_title = title,
    results_title = title,
    prompt_title = "",
    debounce = 600,
    default_text = text
  })
end

local userCommands = {
  -- LiveGrep usage:
  -- 1) call command :LiveGrep [directory_to_search]
  -- 2) type phrase to search in typed directory
  LiveGrep = {
    args = { nargs = "*", complete = "dir" },
    func = function(opts)
      local cwd = opts.fargs[1]
      local text = opts.fargs[2]

      if cwd == nil or cwd == "" then
        M.ListLastCalls(opts.name)
      else
        M.LiveGrep(cwd, text)
      end
    end
  }
}

M.setup = function()
  for cmdName, cmdDef in pairs(userCommands) do
    vim.api.nvim_create_user_command(cmdName, cmdDef.func, cmdDef.args)
  end
end

return M
