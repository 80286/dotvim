local extension = {
  descriptionPrefix = "##"
}

local function desc(str)
  return extension.descriptionPrefix .. " " .. tostring(str)
end

local abstractKeymap = {
  vim_listMyCommands          = { lhs = "<F4>",         desc = desc("Vim: list personal commands") },
  vim_listCommands            = { lhs = "<F1>",         desc = desc("Vim: list commands") },
  vim_saveFile                = { lhs = "<F2>",         desc = desc("Vim: save file") },
  vim_closeFile               = { lhs = "<F3>",         desc = desc("Vim: close file") },
  vim_closeInstant            = { lhs = "<S-F3>",       desc = desc("Vim: close file!") },
  vim_scrollLineUp            = { lhs = "<S-Up>",       desc = desc("Vim: scroll one line up") },
  vim_scrollLineDown          = { lhs = "<S-Down>",     desc = desc("Vim: scroll one line down") },
  vim_skipToNextWindow        = { lhs = "<Tab>",        desc = desc("Vim: skip to next window") },
  vim_pastePrimary            = { lhs = "<S-Insert>",   desc = desc("Vim: paste primary (*) clipboard") },
  vim_copySingleLine          = { lhs = "Y",            desc = desc("Vim: copy line under cursor") },
  vim_toggleDarkLightMode     = { lhs = "<F12>",        desc = desc("Vim: toggle between dark/light background mode") },

  list_buffers                = { lhs = "<C-b>",        desc = desc("List: buffers") },
  list_gitfiles               = { lhs = "<C-n>",        desc = desc("List: gitfiles") },
  list_oldfiles               = { lhs = "<C-h>",        desc = desc("List: oldfiles") },
  list_lspSymbols             = { lhs = "<C-t>",        desc = desc("List: lsp workspace symbols") },

  grep_arctgx_gitGrep         = { lhs = "<leader>q",    desc = desc("Grep: arctgx git-grep") },
  grep_arctgx_rg              = { lhs = "<leader>w",    desc = desc("Grep: arctgx rg") },
  grep_menu                   = { lhs = "<F7>",         desc = desc("Grep: show menu") },
  grep_text                   = { lhs = "<F55>",        desc = desc("Grep: search for cword/vis. selection (lsp -> git -> grep in dir)") }, -- <F55> stands for alt + f7 (tested in xterm+kitty using i<C-R>=getchar()<alt+f7>)

  plugins_openTree            = { lhs = "<F8>",         desc = desc("Plugins: show dirtree") },

  lsp_complete                = { lhs = "<Tab>",        desc = desc("LSP: trigger completion") },
  lsp_showDoc                 = { lhs = "K",            desc = desc("LSP: show doc") },
  lsp_rangeSelect             = { lhs = "<C-s>",        desc = desc("LSP: range select") },
  lsp_gotoDef                 = { lhs = "gd",           desc = desc("LSP: goto def") },
  lsp_typeDef                 = { lhs = "gd",           desc = desc("LSP: goto type def") },
  lsp_showImpl                = { lhs = "gi",           desc = desc("LSP: show impl.") },
  lsp_showRefs                = { lhs = "gy",           desc = desc("LSP: show refs") },
  lsp_rename                  = { lhs = "<F6>",         desc = desc("LSP: rename symbol") },
  lsp_codeAction              = { lhs = "<a-CR>",       desc = desc("LSP: code action") },
  lsp_format                  = { lhs = "<leader>f",    desc = desc("LSP: format selection") },
  lsp_codeLens                = { lhs = "<leader>cl",   desc = desc("LSP: code lens action") },

  lsp_diag                    = { lhs = "<leader>d",    desc = desc("LSP: diagnostics") },
  lsp_diagPrev                = { lhs = "]d",           desc = desc("LSP: diagnostics previous") },
  lsp_diagNext                = { lhs = "[d",           desc = desc("LSP: diagnostics next") },
  lsp_diagQuickfix            = { lhs = "<leader>dqf",  desc = desc("LSP: diagnostics open quickfix window") },

  dap_continue                = { lhs = "<leader>dc",   desc = desc("DAP: run/continue") },
  dap_uiHover                 = { lhs = "<leader>dK",   desc = desc("DAP: ui hover") },
  dap_toggleBreakpoint        = { lhs = "<leader>dt",   desc = desc("DAP: toggle bp") },
  dap_stepOver                = { lhs = "<leader>dso",  desc = desc("DAP: step over") },
  dap_stepInto                = { lhs = "<leader>dsi",  desc = desc("DAP: step into") },
  dap_runLast                 = { lhs = "<leader>dl",   desc = desc("DAP: run last") },
  dap_close                   = { lhs = "<leader>dr",   desc = desc("DAP: close") },

  git_diff_gotoNextHunkInDiff = { lhs = "]c",           desc = desc("Diff view: skip to next git-diff hunk") },
  git_diff_gotoPrevHunkInDiff = { lhs = "[c",           desc = desc("Diff view: skip to prev git-diff hunk") },
  git_diff_previewHunk        = { lhs = "<leader>hp",   desc = desc("Diff: preview git-diff hunk") },
  git_diff_stageHunk          = { lhs = "<leader>hs",   desc = desc("Diff: stage git-diff hunk") },
  git_diff_undoStageHunk      = { lhs = "<leader>hu",   desc = desc("Diff: undo stage of git-diff hunk") },
  git_diff_stageBuffer        = { lhs = "<leader>hS",   desc = desc("Diff: stage changes in current buffer") },
  git_diff_resetHunk          = { lhs = "<leader>hr",   desc = desc("Diff: reset hunk under cursor") },
  git_diff_resetBuffer        = { lhs = "<leader>hR",   desc = desc("Diff: reset changes in current buffer") },
  git_diff_blameLine          = { lhs = "<leader>hb",   desc = desc("Diff: git-blame for line under cursor") },
  git_diff_blameLineToggle    = { lhs = "<leader>tb",   desc = desc("Diff: git-blame for line under cursor (toggle)") },
  git_diff                    = { lhs = "<leader>hd",   desc = desc("Diff: show changes in this buffer") },
  git_diff_toggleDeleted      = { lhs = "<leader>td",   desc = desc("Diff: show deleted hunks") },
}

local function getKeymap(name)
  local keymap = abstractKeymap[name]

  if nil == keymap then
    vim.notify(('%s not defined yet.'):format(name), vim.log.levels.ERROR, {title = 'Keymaps'})
    return nil
  end

  if type(keymap.lhs) == 'string' then
    keymap.lhs = {keymap.lhs}
  end

  return keymap
end

function extension.set(modes, opts, name, rhs)
  local keymap = getKeymap(name)

  if nil == keymap then return end

  if type(modes) == 'string' then
    modes = {modes}
  end

  for _, mode in ipairs(modes) do
    for lhsMode, lhs in pairs(keymap.lhs) do
      if type(lhsMode) ~= 'string' or lhsMode == mode then
        vim.keymap.set(mode, lhs, rhs, vim.tbl_extend('keep', opts or {}, {desc = keymap.desc}))
      end
    end
  end
end

function extension.plug(cmd)
  return ("<Plug>(%s)"):format(cmd)
end

function extension.keymaps()
  return abstractKeymap
end

return extension
