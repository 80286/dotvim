local tActions = require("telescope.actions")
local tActionsState = require("telescope.actions.state")
local tb = require("telescope.builtin")

local privBase = require("80286.base")
local privTelescope = require("80286.telescope")
local arctgxBase = require('arctgx.base')
local arctgx = require('arctgx.telescope')

local M = {}

local function isLspProjectBuffer(bufnr)
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local lspClients = vim.lsp.get_clients({ bufnr = bufnr }) or {}
  return #lspClients ~= 0
end

local function getGitDirOrDirname()
  local bufferCwd = privBase.getBufferCwd()

  return privBase.git_dir(bufferCwd) or vim.fs.dirname(privBase.getRealpath(bufferCwd))
end

local showListOfGrepMethods = function(opts)
  local cmdNames = {}

  for cmdName, _ in pairs(opts.command_defs) do
    table.insert(cmdNames, cmdName)
  end

  table.sort(cmdNames)

  privTelescope.displayTelescopeList({
    inputItems = cmdNames,
    title = ("Grep methods (%s)"):format(("Refs to '%s' in: %s"):format(opts.selection, opts.dir)),
    onSelect = function(selection)
      opts.command_defs[selection.display]()
    end
  })
end

local function isEmptyStr(str)
  return (str == nil or vim.trim(str) == "")
end

local function commonTelescopeOpts(dir, textToSearch, msgGrepType)
  return {
    default_text  = textToSearch,
    preview_title = "",
    prompt_title  = "",
    results_title = isEmptyStr(textToSearch) and (msgGrepType .. " in: " .. dir) or ("%s [%s] in: %s"):format(msgGrepType, textToSearch, dir)
  }
end

local function displayLspRefsToText(msgDir, textToSearch)
  tb.lsp_references(commonTelescopeOpts(msgDir, textToSearch, "LSP refs"))
end

local function displayGrepRefsToText(dir, textToSearch)
  local opts       = commonTelescopeOpts(dir, textToSearch, "Grep refs")
  opts.debounce    = 600
  opts.search_dirs = { dir }
  tb.live_grep(opts)
end

local function displayRefsToGitFiles(dir, textToSearch)
  tb.git_files(commonTelescopeOpts(dir, textToSearch, "Git-ls-files"))
end

local function generalGrep(textToSearch)
  local searchDir = getGitDirOrDirname()

  if isLspProjectBuffer() then
    displayLspRefsToText(searchDir, textToSearch)
  else
    displayGrepRefsToText(searchDir, textToSearch)
  end
end

local function listOldfiles(opts)
  tb.oldfiles({
    prompt_title = opts.prompt_title,
    attach_mappings = function(prompt_bufnr)
      tActions.select_default:replace(function()
        local selection = tActionsState.get_selected_entry()

        if selection ~= nil then
          tActions.close(prompt_bufnr)
          local oldfileRelativePath = selection.display(selection)
          local oldfileDirRealpath = vim.fs.dirname(privBase.getRealpath(oldfileRelativePath))
          local oldfileGitDir = privBase.git_dir(oldfileDirRealpath)

          opts.onSelected(oldfileGitDir)
        end
      end)

      return true
    end
  })
end

local function displayGrepOpsMenu(dir, textToSearch)
  local opts = {
    dir          = dir,
    selection    = textToSearch,
    command_defs = {
      ["a0: LiveGrep-inDir"] = function() privTelescope.listDirs(function(selectedDir) vim.cmd(("LiveGrep %s %s"):format(selectedDir, textToSearch)) end) end,
      ["a1: git-grep"]  = function() displayGrepRefsToText(dir, textToSearch) end,
      ["a2: git-files"] = function() displayRefsToGitFiles(dir, textToSearch) end,
      ["a3: lsp_refs"]  = function() displayLspRefsToText(dir, textToSearch) end,

      ["x1: git-grep"]  = function() arctgx.gitGrep(textToSearch, true) end,
      ["x2: rg"]        = function() arctgx.rgGrep(textToSearch, true) end,

      ["b1: grep-text-in-oldfile-dir"] = function()
        listOldfiles({
          prompt_title = "Grep for text in dir of selected oldfile",
          onSelected = function(gitDirOfSelectedOldfile) displayGrepRefsToText(gitDirOfSelectedOldfile, textToSearch) end
        })
      end,

      ["b2: git-filename-in-oldfile-dir"] = function()
        listOldfiles({
          prompt_title = "Find filename in gitdir of selected oldfile",
          onSelected = function(gitDirOfSelectedOldfile) displayRefsToGitFiles(gitDirOfSelectedOldfile, textToSearch) end
        })
      end,

      ["b3: grep"] = function() generalGrep(textToSearch) end
    }
  }

  showListOfGrepMethods(opts)
end

local function getCword()
  return vim.fn.expand("<cword>")
end

M.displayGrepOpsForCword           = function() displayGrepOpsMenu(getGitDirOrDirname(), getCword()) end
M.displayGrepOpsForVisualSelection = function() displayGrepOpsMenu(getGitDirOrDirname(), arctgxBase.getVisualSelection()) end

M.grepVisual                       = function() generalGrep(arctgxBase.getVisualSelection()) end
M.grepCword                        = function() generalGrep(getCword()) end

return M
