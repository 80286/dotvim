local plugin = {}

local function getListOfAllPluginNames(pluginDir)
  local pluginPaths = vim.fn.globpath(pluginDir, '*', 1, 1)
  local plugins = {}

  for _, path in ipairs(pluginPaths) do
    table.insert(plugins, vim.fn.fnamemodify(path, ':t'))
  end

  return plugins
end

local function loadSingleConfiguration(pluginConfigDir, pluginName)
  local luaPluginConfigFilePath = pluginConfigDir .. '/' .. pluginName:gsub('%.lua$', '') .. '.lua'
  local luaPluginConfigRealPath = vim.loop.fs_realpath(luaPluginConfigFilePath)

  if nil ~= luaPluginConfigRealPath then
    dofile(luaPluginConfigRealPath)
  end
end

local function getRealRuntimePaths()
  local runtimePaths = {}

  for _, runtimePath in ipairs(vim.opt.runtimepath:get()) do
      local realRuntimePath = vim.loop.fs_realpath(runtimePath)

      if realRuntimePath ~= "" then
          table.insert(runtimePaths, realRuntimePath)
      end
  end

  return runtimePaths
end

function plugin.loadCustomConfiguration(pluginDirs, pluginConfigDir)
  local runtimePaths = getRealRuntimePaths()

  for _, pluginDir in ipairs(pluginDirs) do
    for _, pluginName in ipairs(getListOfAllPluginNames(pluginDir)) do
      local p = vim.loop.fs_realpath(pluginDir .. '/' .. pluginName)
      local runtimePathContainsPluginDir = vim.tbl_contains(runtimePaths, p)

      if runtimePathContainsPluginDir then
        loadSingleConfiguration(pluginConfigDir, pluginName)
      end
    end
  end
end

return plugin
