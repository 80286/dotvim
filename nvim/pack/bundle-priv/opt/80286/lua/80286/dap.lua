local M = {}

M.closeDapOr = function(doSthElse)
  local dap = require("dap")

  if dap and dap.session() then
    dap.close()
    dap.clear_breakpoints()
    vim.api.nvim_exec_autocmds('User', {pattern = 'DAPClean', modeline = false})
  else
    if doSthElse then
      doSthElse()
    end
  end
end

M.closeDap = function() M.closeDapOr(nil) end

return M
