local M = {}
local api = vim.api

local hlGroup = api.nvim_create_augroup('LspDocumentHighlight', {clear = true})

local setup_higlight_for_client = function(client, bufnr)
  if client.server_capabilities.documentHighlightProvider then
    api.nvim_create_autocmd({'CursorMoved', 'CursorMovedI'}, {
      group = hlGroup,
      buffer = bufnr,
      callback = function (args) vim.lsp.buf.clear_references(args.buf) end
    })
    api.nvim_create_autocmd({'CursorHold', 'CursorHoldI'}, {
      group = hlGroup,
      buffer = bufnr,
      callback = vim.lsp.buf.document_highlight
    })

    local hlMap = {
       LspReferenceRead  = "#5B532C",
       LspReferenceText  = "#2C3C5B",
       LspReferenceWrite = "#4B281D",
    }

    for key, value in pairs(hlMap) do
      api.nvim_set_hl(0, key, { bg = value })
    end

    api.nvim_create_autocmd({'LspDetach'}, {
      group = hlGroup,
      buffer = bufnr,
      callback = function (args)
        vim.lsp.buf.clear_references(args.buf)

        local supported = false

        for i, client in ipairs(vim.lsp.get_clients({bufnr = bufnr})) do
          if (client.id ~= args.data.client_id) and client.supports_method('textDocument/documentHighlight') then
            supported = true
          end
        end

        if supported then
          return
        end

        api.nvim_clear_autocmds {
          group = hlGroup,
          buffer = args.buf,
        }
      end,
    })
  end
end

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
M.on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.bo[bufnr].omnifunc = 'v:lua.vim.lsp.omnifunc'

  local km = require("80286.keymap")
  local function set(keyId, func)
    local opts = { noremap = true, silent = true, buffer = bufnr }
    km.set({"n"}, opts, keyId, func)
  end

  set("lsp_gotoDef",    function() vim.lsp.buf.definition() end)
  set("lsp_showDoc",    function() vim.lsp.buf.hover() end)
  set("lsp_showImpl",   function() vim.lsp.buf.implementation() end)
  set("lsp_typeDef",    function() vim.lsp.buf.type_definition() end)
  set("lsp_rename",     function() vim.lsp.buf.rename() end)
  set("lsp_codeAction", function() vim.lsp.buf.code_action() end)
  set("lsp_showRefs",   function() vim.lsp.buf.references() end)
  set("lsp_format",     function() vim.lsp.buf.format() end)
  set("lsp_codeLens",   function() vim.lsp.codelens.run() end)

  set("lsp_diag",       function() vim.diagnostic.open_float() end)
  set("lsp_diagNext",   function() vim.diagnostic.goto_prev() end)
  set("lsp_diagPrev",   function() vim.diagnostic.goto_next() end)
  set("lsp_diagQuickfix", function() vim.diagnostic.setqflist() end)

  setup_higlight_for_client(client, bufnr)
  vim.notify(("%s started"):format(client.name))
end

return M
