local M = {}

local function setBasicSettings()
  vim.g.mapleader = ','
  vim.bo.softtabstop = -1
  vim.bo.tabstop = 2
  vim.bo.shiftwidth = 2
  vim.wo.number = true
  vim.wo.numberwidth = 1
  vim.go.splitbelow = true

  vim.opt.expandtab = true
  vim.opt.shiftwidth = 2
  vim.opt.updatetime = 300
  vim.opt.diffopt:append('filler,vertical')

  vim.cmd.colorscheme("lunaperche")
  vim.o.background = "dark"
end

local function setKeyMappings()
  local nvi = {"v", "n", "i"}
  local ni = {"n", "i"}
  local nv = {"n", "v"}
  local n = {"n"}
  local v = {"v"}
  local opts = {silent = true}

  local tb = require("telescope.builtin")
  local privKeymap = require("80286.keymap")
  local privBase = require("80286.base")
  local privGrep = require("80286.grep")

  local arctgxBase = require("arctgx.base")
  local arctgxTelescope = require('arctgx.telescope')

  local dap = require("dap")
  local plug = privKeymap.plug

  local function telescopeCall(telescopeFunc, title)
    return function() telescopeFunc({
      prompt_title = "",
      preview_title = type(title) == "string" and title or title()
    }) end
  end

  local function set(modes, keyname, rhs, extraOpts)
    extraOpts = extraOpts or opts
    privKeymap.set(modes, extraOpts, keyname, rhs)
  end

  set(n,     "vim_listMyCommands",   function() tb.keymaps({ results_title = "Priv keymaps", prompt_title = "", default_text = ("'%s "):format(privKeymap.descriptionPrefix) }) end)
  set(nvi,   "vim_listCommands",     tb.builtin)
  set(ni,    "vim_saveFile",         vim.cmd.update)
  set(n,     "vim_closeFile",        function() require("80286.dap").closeDapOr(vim.cmd.quit) end)
  set(n,     "vim_closeInstant",     function() vim.cmd.quit {bang = true} end)
  set(n,     "vim_scrollLineUp",     "<C-y>")
  set(n,     "vim_scrollLineDown",   "<C-e>")
  set(n,     "vim_skipToNextWindow", "<C-w>w")
  set({"!"}, "vim_pastePrimary",     "<MiddleMouse>")
  set(n,     "vim_copySingleLine",   "yy")
  set(n,     "vim_toggleDarkLightMode", function() vim.o.background = vim.o.background == "dark" and "light" or "dark" end)

  set(n,     "list_buffers",         telescopeCall(tb.buffers, "Buffers"))
  set(n,     "list_gitfiles",        telescopeCall(tb.git_files, function() return ("Git files in repo: %s"):format(privBase.git_dir(privBase.getBufferCwd())) end))
  set(n,     "list_oldfiles",        telescopeCall(tb.oldfiles, "Oldfiles"))
  set(n,     "list_lspSymbols",      telescopeCall(tb.lsp_definitions, "LspSymbols"))

  local function setGrepOperator(keyname, grepOperatorFunc)
    set(nv, keyname, function() arctgxBase.setOperatorfunc(grepOperatorFunc) return 'g@' end, {expr = true})
  end

  setGrepOperator("grep_arctgx_gitGrep", arctgxTelescope.gitGrepOperator)
  setGrepOperator("grep_arctgx_rg",      arctgxTelescope.rgGrepOperator)

  set(n,     "grep_menu",            function() privGrep.displayGrepOpsForCword() end)
  set(v,     "grep_menu",            function() privGrep.displayGrepOpsForVisualSelection() end)
  set(n,     "grep_text",            function() privGrep.grepCword() end)
  set(v,     "grep_text",            function() privGrep.grepVisual() end)

  set(n,     "plugins_openTree",     plug("ide-tree-focus-current-file"))

  -- goto: nvim-lsp-on-attach.lua to find LSP key mappings

  set(n,     "dap_continue",         function() dap.continue() end)
  set(n,     "dap_uiHover",          function() require("dap.ui.widgets").hover() end)
  set(n,     "dap_toggleBreakpoint", function() dap.toggle_breakpoint() end)
  set(n,     "dap_stepOver",         function() dap.step_over() end)
  set(n,     "dap_stepInto",         function() dap.step_into() end)
  set(n,     "dap_runLast",          function() dap.run_last() end)
  set(n,     "dap_close",            require("80286.dap").closeDap)
end

M.setup = function()
  setBasicSettings()
  setKeyMappings()
  require("80286.user-commands").setup()
end

return M
