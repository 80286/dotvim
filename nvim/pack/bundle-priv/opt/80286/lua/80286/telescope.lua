local ext = {}

local tActions = require("telescope.actions")
local tActionsState = require("telescope.actions.state")
local tConf = require("telescope.config").values
local tFinders = require("telescope.finders")
local tMakeEntry = require("telescope.make_entry")
local tPickers = require("telescope.pickers")
local privBase = require("80286.base")

local function simpleEntryFunc(line)
  local sourced_file = require("plenary.debug_utils").sourced_filepath()

  return {
    ordinal = line,
    display = line,
    filename = sourced_file,
  }
end

local function simpleFinder(inputItems, opts)
  local entryFunc = opts.entryFunc or simpleEntryFunc

  return tFinders.new_table {
      results = inputItems,
      entry_maker = function(line)
        return tMakeEntry.set_default_entry_mt(entryFunc(line), opts)
      end
    }
end

function ext.displayTelescopeList(opts)
  local title = opts.title
  local inputItems = opts.inputItems

  tPickers.new(opts, {
    results_title = title,
    prompt_title = title,
    finder = simpleFinder(inputItems, opts),
    sorter = tConf.file_sorter(opts),
    attach_mappings = function(prompt_bufnr)
      tActions.select_default:replace(function()
        local selection = tActionsState.get_selected_entry()

        if selection ~= nil then
          tActions.close(prompt_bufnr)
          opts.onSelect(selection)
        end
      end)

      return true
    end,
  }):find()
end

local function getCurrentPicker(bufnr)
  return require("telescope.actions.state").get_current_picker(bufnr)
end

local function getListOfEntries(bufnr)
  local picker = getCurrentPicker(bufnr)
  local entries = {}

  for entry in picker.manager:iter() do
    local text = entry.text

    if not text then
      text = type(entry.value) == "table" and entry.value.text or entry.value
    end

    table.insert(entries, text)
  end

  return entries
end

function ext.listDirs(onSelect, opts)
  opts = { debounce = 600 } or opts

  local function getManualPathSelectionFinder()
    return tFinders.new_job(function(prompt)
      if not prompt or prompt == nil then return end

      local lastPathSepIdx = string.find(prompt, "/[^/]*$")
      local fixedPrompt = string.sub(prompt, 1, lastPathSepIdx)

      return { "fdfind", "-H", "-t", "d", "-d", "1", ".", fixedPrompt }
    end)
  end

  local function getLastLiveGrepDirFinder()
    local liveGrepCalls = privBase.getCommandHistory(function(cmd) return vim.startswith(cmd, "LiveGrep") end)
    local lastUsedDirs = {}

    for _, cmd in ipairs(liveGrepCalls) do
      local cmdParts = vim.split(cmd, " ")

      if cmdParts and #cmdParts > 1 then
        local cmdPath = vim.trim(cmdParts[2])

        if cmdPath ~= "" then
          table.insert(lastUsedDirs, cmdPath)
        end
      end
    end

    lastUsedDirs = privBase.tableGetUniqueItems(lastUsedDirs)

    return simpleFinder(lastUsedDirs, {refresh_prompt = true})
  end

  tPickers.new(opts, {
    prompt_title = "Find Directory",
    finder = getLastLiveGrepDirFinder(),
    sorter = tConf.generic_sorter(opts),
    attach_mappings = function(prompt_bufnr, map)
      tActions.select_default:replace(function()
        local selection = tActionsState.get_selected_entry()

        if selection ~= nil then
          tActions.close(prompt_bufnr)
          onSelect(selection.display)
        end
      end)

      local function tryToCompleteDir(_prompt_bufnr)
        local picker = getCurrentPicker(_prompt_bufnr)
        local currentPrompt = picker:_get_prompt()
        local entries = getListOfEntries(_prompt_bufnr)
        local minPrefix = privBase.getMinPrefix(entries, currentPrompt)

        if minPrefix then
          picker:set_prompt(minPrefix)
        end
      end

      local function switchToManualPathSelection(_prompt_bufnr)
        local picker = getCurrentPicker(_prompt_bufnr)
        picker:reset_prompt()
        picker:refresh(getManualPathSelectionFinder())
      end

      local modes = {"i", "n"}
      map(modes, "<Tab>", tryToCompleteDir,            { desc = "Try to complete directory"})
      map(modes, "<C-h>", switchToManualPathSelection, { desc = "Switch to manual path selection"})

      return true
    end
  }):find()
end

return ext
