-- https://github.com/nvim-treesitter/nvim-treesitter

require'nvim-treesitter.configs'.setup {
  ensure_installed = {'dockerfile', 'javascript', 'json', 'jsonc', 'yaml', 'cpp', 'scala'},
  highlight = {
    enable = true,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = ",]",
      node_incremental = ",]",
      -- scope_incremental = ",]",
      node_decremental = ",[",
    },
  },
  indent = {
    enable = true,
  },
}

vim.cmd([[
  set foldexpr=nvim_treesitter#foldexpr()
]])
