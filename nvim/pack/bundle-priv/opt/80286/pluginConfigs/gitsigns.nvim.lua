require('gitsigns').setup({
  attach_to_untracked = false,
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns

    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    local km = require("80286.keymap")
    local function set(modes, keyname, rhs, opts) km.set(modes, opts or {}, keyname, rhs) end

    set("n", "git_diff_gotoNextHunkInDiff", function()
      if vim.wo.diff then return ']c' end
      vim.schedule(function() gs.next_hunk() end)
      return '<Ignore>'
    end, { expr = true })
    set("n", "git_diff_gotoPrevHunkInDiff", function()
      if vim.wo.diff then return '[c' end
      vim.schedule(function() gs.prev_hunk() end)
      return '<Ignore>'
    end, { expr = true })

    set("n", "git_diff_previewHunk",     gs.preview_hunk)
    set("n", "git_diff_stageHunk",       gs.stage_hunk)
    set("v", "git_diff_stageHunk",       function() gs.stage_hunk { vim.fn.line('.'), vim.fn.line('v') } end)
    set("n", "git_diff_stageBuffer",     gs.stage_buffer)
    set("n", "git_diff_undoStageHunk",   gs.undo_stage_hunk)
    set("n", "git_diff_resetHunk",       gs.reset_hunk)
    set("v", "git_diff_resetHunk",       function() gs.reset_hunk { vim.fn.line('.'), vim.fn.line('v') } end)
    set("n", "git_diff_resetBuffer",     gs.reset_buffer)
    set("n", "git_diff_blameLine",       function() gs.blame_line { full = true } end)
    set("n", "git_diff_blameLineToggle", gs.toggle_current_line_blame)
    set("n", "git_diff",                 gs.diffthis)
    set("n", "git_diff_toggleDeleted",   gs.toggle_deleted)

    -- Text object
    map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
  end
})
