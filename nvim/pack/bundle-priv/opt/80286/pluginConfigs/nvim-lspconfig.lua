-- Language Server Protocol settings (https://langserver.org/)

local nvim_lsp = require('lspconfig')

local on_attach = require('80286.nvim-lsp-on-attach').on_attach

local capabilities = vim.lsp.protocol.make_client_capabilities()
local cmpNvimLspOk, cmpNvimLsp = pcall(require, 'cmp_nvim_lsp')
if cmpNvimLspOk then
  capabilities = cmpNvimLsp.default_capabilities(capabilities)
end
capabilities.textDocument.completion.completionItem.snippetSupport = true

nvim_lsp.lua_ls.setup {
  autostart = true,
  capabilities = capabilities,
  on_attach = on_attach,
  settings = {
    Lua = {
      runtime = {
        path = {
          'lua/?.lua',
          'lua/?/init.lua',
          '?.lua',
          '?/init.lua',
        },
        version = 'LuaJIT',
        pathStrict = true,
      },
      workspace = {
        -- library = getLuaRuntime(),
        maxPreload = 10000,
        preloadFileSize = 10000,
        library = vim.api.nvim_get_runtime_file('', true),
        checkThirdParty = false,
      },
      completion = {
        showWord = 'Disable',
        postfix = '.',
      },
      diagnostics = {
        globals = {'vim'},
        neededFileStatus = {
          ['codestyle-check'] = 'Any',
        },
      },
      telemetry = {
        enable = false,
      },
      window = {
        statusBar = false,
      },
    },
  },
}

-- Diagnostic config:
vim.diagnostic.config({
  virtual_text = false,
  underline = false,
  float = {
    border = 'rounded',
  },
  signs = {
    text = {
      [vim.diagnostic.severity.ERROR] = '',
      [vim.diagnostic.severity.WARN] = '󰀪',
      [vim.diagnostic.severity.INFO] = '',
      [vim.diagnostic.severity.HINT] = '',
    },
  },
  severity_sort = true,
})

local servers = { 'clangd', 'rust_analyzer' }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    capabilities = capabilities,
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    }
  }
end
