-- examples for your init.lua
local api = require('nvim-tree.api')

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

-- local textOnlyGlyphs = {
--     default = "",
--     symlink = "",
--     bookmark = "B",
--     modified = "",
--     folder = {
--       arrow_closed = "|",
--       arrow_open = ">",
--       default = "+",
--       open = "+",
--       empty = "",
--       empty_open = "",
--       symlink = "",
--       symlink_open = "",
--     },
--     git = {
--       unstaged = "",
--       staged = "",
--       unmerged = "",
--       renamed = "",
--       untracked = "",
--       deleted = "",
--       ignored = "",
--     },
-- }

local function open_tab_silent(node)
  api.node.open.tab(node)
  vim.cmd.tabprev()
end

local function on_attach(bufnr)
  local function map(key, func, desc)
    local opts = { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
    vim.keymap.set('n', key, func, opts)
  end

  api.config.mappings.default_on_attach(bufnr)

  vim.keymap.del('n', '<Tab>', { buffer = bufnr })

  map('<Left>',  api.node.navigate.parent,       "prev")
  map('<Right>', api.node.open.preview,          "next")
  map('U',       api.tree.change_root_to_parent, "dir_up")
  map('I',       api.tree.toggle_hidden_filter,  "toggle_hidden")
  map('t',       api.node.open.tab,              "tabnew")
  map('T',       open_tab_silent,                "tabnew_silent")
end

-- OR setup with some options
require("nvim-tree").setup({
  on_attach = on_attach,
  disable_netrw = false,
  sort_by = "case_sensitive",
  view = {
    width = 30,
  },
  renderer = {
    group_empty = true,
    -- icons = {
    --     show = { folder_arrow = false },
    --     glyphs = textOnlyGlyphs
    -- }
  },
  filters = {
    dotfiles = false,
  },
})

local function focusOnFile()
  local bufName = vim.api.nvim_buf_get_name(0)
  local directory = vim.fn.fnamemodify(bufName, ":p:h")

  api.tree.open(directory)
  api.live_filter.clear()
  api.tree.find_file(bufName)
end

local keymap = require('vim.keymap')
keymap.set({'n'}, '<Plug>(ide-tree-focus-current-file)', focusOnFile)
