local dapui = require 'dapui'

dapui.setup()

vim.keymap.set("n", '<leader>de', function() dapui.eval(nil, {enter = true, context = 'repl'}) end)
