local dap = require("dap")

local showDbgOutputWindow = function()
  local _, dbgOutputWindow = dap.repl.open()

  vim.api.nvim_win_set_height(dbgOutputWindow, 14)
end

-- Show debug output after start of run/debug call regardless of source (codelens/dap.continue() etc.):
dap.listeners.before["launch"]["nvim-dap-cfg"] = function(_)
  -- Put list of all bps in quickfix:
  dap.list_breakpoints(false)
  local quickFixListItems = vim.fn.getqflist()
  local noBreakpointsAreSet = (quickFixListItems and #quickFixListItems or 0) == 0

  if noBreakpointsAreSet then
    showDbgOutputWindow()
  end
end
