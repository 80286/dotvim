local function loadPluginConfigs()
    local configDir = vim.fn.expand('<sfile>:p:h')
    local pluginConfigDir = vim.fn.simplify(configDir .. '/../pluginConfigs/')

    require("80286.plugin").loadCustomConfiguration(vim.g.bundle_dirs, pluginConfigDir)
end

loadPluginConfigs()
