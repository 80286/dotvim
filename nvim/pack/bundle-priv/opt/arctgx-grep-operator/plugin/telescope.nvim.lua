local keymap = require('vim.keymap')
local arctgx = require('arctgx.telescope')
local base = require('arctgx.base')

local function plug(cmd) return ("<Plug>(%s)"):format(cmd) end
keymap.set('n', plug("ide-grep-git"),                    function() arctgx.gitGrep('', false, false) end)
keymap.set('n', plug("ide-grep-files"),                  function() arctgx.rgGrep('', false, false) end)

keymap.set('v', plug("ide-git-string-search-operator"),  function() arctgx.gitGrep(base.getVisualSelection(), true) end)
keymap.set('v', plug("ide-grep-string-search-operator"), function() arctgx.rgGrep(base.getVisualSelection(), true) end)
-- keymap.set('v', plug("ide-git-files-search-operator"),   function() arctgx.filesGit(base.getVisualSelection(), true) end)
-- keymap.set('v', plug("ide-files-search-operator"),       function() arctgx.filesAll(base.getVisualSelection(), true) end)

local opPrefix = "v:lua.require'arctgx.telescope'"
keymap.set('n', plug("ide-git-string-search-operator"),  function() base.runOperator(opPrefix .. ".git_grep_operator")  end)
keymap.set('n', plug("ide-grep-string-search-operator"), function() base.runOperator(opPrefix .. ".rg_grep_operator")   end)
-- keymap.set('n', plug("ide-git-files-search-operator"),   function() base.runOperator(opPrefix .. ".files_git_operator") end)
-- keymap.set('n', plug("ide-files-search-operator"),       function() base.runOperator(opPrefix .. ".files_all_operator") end)
