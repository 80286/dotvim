local function setupDirs()
    local configDir = vim.fn.expand('<sfile>:p:h:h')

    vim.env['XDG_CONFIG_HOME'] = configDir
    vim.env['XDG_DATA_HOME'] = configDir .. '/.xdg/data'
    vim.env['XDG_STATE_HOME'] = configDir .. '/.xdg/state'
    vim.env['XDG_CACHE_HOME'] = configDir .. '/.xdg/cache'

    local stdPathConfig = vim.fn.stdpath('config')

    vim.opt.runtimepath:prepend(stdPathConfig)
    vim.opt.packpath:prepend(stdPathConfig)
    vim.opt.runtimepath:append(stdPathConfig .. '/after')

    vim.g.bundle_dirs = {
         vim.fn.expand(stdPathConfig .. '/pack/bundle/opt'),
         vim.fn.expand(stdPathConfig .. '/pack/bundle-priv/opt'),
    }
    vim.g.initialVimDirectory = stdPathConfig
end

local function loadPackages()
    local packages = {
        "Comment.nvim",        	              -- neovim comment plugin
        "cmp-nvim-lsp",                       -- ???
        "nvim-lspconfig",                     -- configs for nvim LSP client ??
        "nvim-cmp",                           -- LSP completion
        "nvim-treesitter",                    -- treesitter syntax highlighting
        "nvim-treesitter-textobjects",        -- treesitter ext. (inc/dec selection, other textobject operators)
        "nvim-autopairs",

        "plenary.nvim",                       -- lua tools?
        "telescope.nvim",
        "telescope-fzf-native.nvim",
        "telescope-ui-select.nvim",
        "nvim-metals",                        -- scala LSP (metals) ext.
        "nvim-bqf",                           -- enhanced quickfix

        "nord.nvim",                          -- colorscheme
        "monokai-pro.nvim",                   -- colorscheme
        "vim-moonfly-colors",                 -- colorscheme

        "lualine.nvim",                       -- colored statusline ext.
        "neo-tree.nvim",                      -- file explorer

        "noice.nvim",                         -- replacement for UI - move messages to floating windows
        "nui.nvim",                           -- required by: noice.nvim: UI component lib
        "vim-fugitive",

        "nvim-dap",
        "nvim-dap-ui",
        "nvim-dap-tab",
        "nvim-nio",                           -- required by: nvim-dap-ui

        "gitsigns.nvim",
        "LuaSnip",
        "cmp_luasnip",

        "80286",
        "arctgx-grep-operator"
    }

    for _, packageName in ipairs(packages) do
      vim.cmd.packadd({args = {packageName}, bang = true})
    end

    require('Comment').setup()
end

setupDirs()
loadPackages()
require("80286.init").setup()
