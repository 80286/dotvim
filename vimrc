let mapleader = ','

let g:initialVimDirectory = expand('<sfile>:p:h')
let &runtimepath = g:initialVimDirectory.','.&runtimepath.','.g:initialVimDirectory.'/after'
let &packpath = &runtimepath

packadd! 80286
" packadd! indentLine
" packadd! snippets
" packadd! snippets.80286

packadd! tabular

packadd! vim-commentary
packadd! vim-dispatch
packadd! vim-fugitive
packadd! vim-surround
packadd! vim-test

packadd! arctgx-grep-operator
packadd! coc.nvim
packadd! fzf
packadd! fzf.vim
packadd! nerdtree
packadd! molokai
packadd! ultisnips
