### FAQ

#### Inspecting ids of key mappings

1. Type in normal mode `i<C-R>=getchar()`
2. Press combination of keys you're going to check.
3. After that you should see output like `<F55>` (after pressing alt+f7 inside xterm+nvim)

Example codes:

- Typing f7       -> `<F7>`  (checked in xterm & kitty)
- Typing alt+f7   -> `<F55>` (checked in xterm & kitty)
- Typing shift+f7 -> `<F19>` (checked in xterm & kitty)

xterm: some keys may not work correctly - check `xterm*translations` setting inside `~/.Xresources`

Refs:
[https://github.com/neovim/neovim/issues/21399]
[https://github.com/kylechui/nvim-surround/issues/178]
